<?php


namespace App\ViewComposers;

use App\Models\Community\Community;
use Illuminate\View\View;

class MenuComposer
{
    public function compose(View $view): void
    {
        if(auth()->guard(Community::GUARD_NAME)->check()) {
            $view->with([
                Community::GUARD_NAME => auth(Community::GUARD_NAME)->user()
            ]);
        }
    }
}
