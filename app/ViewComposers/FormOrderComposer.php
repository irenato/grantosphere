<?php


namespace App\ViewComposers;

use App\Models\Community\Community;
use Illuminate\View\View;

class FormOrderComposer
{
    public function compose(View $view): void
    {
        $view->with([
            'values' => [
                'views' => __('frontend.by_view'),
                'updated_at' => __('frontend.by_update'),
                'title' => __('frontend.by_title'),
            ],
            'category' => request()->get('category', ''),
            'routeName' => request()->route()->getName()
        ]);
    }
}
