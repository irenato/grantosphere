<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }


    public function boot()
    {
        View::composer('frontend.parts.menu', 'App\ViewComposers\MenuComposer');
        View::composer('frontend.parts.forms.order-results', 'App\ViewComposers\FormOrderComposer');
    }
}
