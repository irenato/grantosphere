<?php
    /**
     * Created by PhpStorm.
     * User: user
     * Date: 30.10.18
     * Time: 10:51
     */

    namespace App\Repositories\Region;

    use App\Models\Region\Region;
    use Illuminate\Http\Request;

    class RegionRepository
    {
        protected $request;

        public function __construct(Request $request)
        {
            $this->request = $request;
        }

        public function store(): Region
        {
            return Region::create($this->request->all());
        }

        public function update($id): Region
        {
            $category = Region::findOrFail($id);
            $category->update($this->request->all());
            return $category;
        }
    }
