<?php

namespace App\Repositories\Grant;

use App\Models\Grant\Grant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class GrantRepository
{
    protected $request;
    protected const LIMIT = 50;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index(){
        $basicGrants = Grant::with(['donor', 'categories_grants']);
        foreach ($this->request->all() as $key => $value) {
            if (isset($value)) {
                switch ($key) {
                    case 'page':
                    case 'limit':
                        break;
                    case 'donor':
                        $basicGrants->whereHas('donor', function ($query) use ($value) {
                            $query->where('donor_id', $value);
                        });
                        break;
                    case 'category':
                        $basicGrants->whereHas('categories_grants', function ($query) use ($value) {
                            $query->where('category_id', $value);
                        });
                        break;
                    default:
                        if (Schema::hasColumn('grants', $key)) {
                            $basicGrants
                                ->where($key, 'like', $value . "%");
                            break;
                        }
                };
            };
        };
        return $basicGrants->latest('updated_at')->paginate(self::LIMIT);
    }

    public function store(): Grant
    {
        $data = $this->request->all();
        $data['slug'] = $data['slug'] ?? Str::slug($data['title']);
        $grant = Grant::create($data);
        $this->attachImages($grant);
        $grant->categories_grants()->sync($this->request->categories_grants);
        return $grant;
    }

    public function update($id): Grant
    {
        $data = $this->request->all();
        $data['slug'] = $data['slug'] ?? Str::slug($data['title']);
        $grant = Grant::findOrFail($id);
        $grant->categories_grants()->sync($this->request->categories_grants);
        $grant->update($data);
        $this->attachImages($grant);
        return $grant;
    }

    protected function attachImages(Grant $grant): bool
    {
        if ($this->request->has('image')) {
            $grant->image
                = md5($this->request->image->getClientOriginalName())
                . '.'
                . $this->request->image->getClientOriginalExtension();
            if ($this->request->has('oldImage')) {
                File::delete(
                    storage_path('public/images/grants/' . $grant->id . '/' . $grant->oldImage)
                );
            }
            Storage::disk('grants')
                ->put(
                    $grant->id . '/' .
                    $grant->image, File::get($this->request->image)
                );
            return $grant->update();
        }
        return false;
    }
}
