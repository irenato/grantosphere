<?php
    /**
     * Created by PhpStorm.
     * User: user
     * Date: 30.10.18
     * Time: 10:51
     */

    namespace App\Repositories\User;

    use App\Models\User;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Hash;

    class UserRepository
    {
        protected $request;

        public function __construct(Request $request)
        {
            $this->request = $request;
        }

        public function store(): User
        {
            $user           = new User();
            $user->name  = $this->request->name;
            $user->email = $this->request->email;
            $user->password = Hash::make($this->request->password);
            $user->save();
            return $user;
        }

        public function update($id): User
        {
            $user        = User::find($id);
            $user->name  = $this->request->name;
            $user->email = $this->request->email;
            if (isset($this->request->password)) {
                $user->password = Hash::make($this->request->password);
            }
            $user->update();
            return $user;
        }
    }
