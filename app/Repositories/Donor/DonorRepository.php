<?php

namespace App\Repositories\Donor;

use App\Models\Donor\Donor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class DonorRepository
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function store(): Donor
    {
        $data = $this->request->all();
        $data['slug'] = Str::slug($data['title']);
        $donor = Donor::create($data);
        $this->attachImages($donor);
        return $donor;
    }

    public function update($id): Donor
    {
        $data = $this->request->all();
        $data['slug'] = Str::slug($data['title']);
        $donor = Donor::findOrFail($id);
        $donor->update($data);
        $this->attachImages($donor);
        return $donor;
    }

    protected function attachImages(Donor $donor): bool
    {
        if ($this->request->has('image')) {
            $donor->image
                = md5($this->request->image->getClientOriginalName())
                . '.'
                . $this->request->image->getClientOriginalExtension();
            if ($this->request->has('oldImage')) {
                File::delete(
                    storage_path('public/images/donors/' . $donor->id . '/' . $donor->oldImage)
                );
            }
            Storage::disk('donors')
                ->put(
                    $donor->id . '/' .
                    $donor->image, File::get($this->request->image)
                );
            return $donor->update();
        }
        return false;
    }
}
