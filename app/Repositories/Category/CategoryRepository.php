<?php

    namespace App\Repositories\Category;


    use App\Models\Category\Category;
    use Illuminate\Http\Request;
    use Illuminate\Support\Str;

    class CategoryRepository
    {
        protected $request;

        public function __construct(Request $request)
        {
            $this->request = $request;
        }

        public function store(): Category
        {
            $data = $this->request->all();
            $data['slug'] = Str::slug($data['title']);
            return Category::create($data);
        }

        public function update($id): Category
        {
            $data = $this->request->all();
            $data['slug'] = Str::slug($data['title']);
            $category = Category::findOrFail($id);
            $category->update($data);
            return $category;
        }
    }
