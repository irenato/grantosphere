<?php

namespace App\Repositories\Community;

use App\Mail\RegisterLinkMail;
use App\Models\Community\Community;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;

class CommunityRepository
{
    protected $request;
    protected const LIMIT = 50;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index(): LengthAwarePaginator
    {
        return $this->filterCommunities()->latest('updated_at')->paginate(self::LIMIT);
    }

    public function store(): Community
    {
        $data = $this->request->all();
        $data['activation_hash'] = md5($this->request->email);
        $community = Community::create($data);
        $this->attachImages($community);
        return $community;
    }

    public function update($id): Community
    {
        $community = Community::findOrFail($id);
        $community->update($this->request->all());
        $this->attachImages($community);
        return $community;
    }

    protected function attachImages(Community $community): bool
    {
        if ($this->request->has('logo')) {
            $community->logo
                = md5($this->request->logo->getClientOriginalName())
                . '.'
                . $this->request->logo->getClientOriginalExtension();
            if ($this->request->has('oldLogo')) {
                File::delete(
                    storage_path('public/images/communities/' . $community->id . '/' . $community->oldLogo)
                );
            }
            Storage::disk('communities')
                ->put(
                    $community->id . '/' .
                    $community->logo, File::get($this->request->logo)
                );
            return $community->update();
        }
        return false;
    }

    public function sendRegLink(): Collection
    {
        $communities = $this
            ->filterCommunities()
            ->whereNotNull('email')
            ->get();
        foreach ($communities as $community) {
            try {
                Mail::to($community->email)->send(new RegisterLinkMail($community));
            } catch (\Exception $e) {
                 logger($e->getMessage());
            }
        }
        return $communities;
    }

    protected function filterCommunities(): Builder
    {
        $communities = Community::with('region');
        foreach ($this->request->all() as $key => $value) {
            if (isset($value)) {
                switch ($key) {
                    case 'page':
                    case 'limit':
                        break;
                    case 'region_id':
                        $communities->where('region_id', $value);
                        break;
                    default:
                        if (Schema::hasColumn('communities', $key)) {
                            $communities
                                ->where($key, 'like', $value . "%");
                            break;
                        }
                };
            };
        };
        return $communities;
    }
}
