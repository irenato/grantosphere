<?php

namespace App\Models\Donor;

use App\Helpers\Helpers;
use App\Models\Grant\Grant;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Storage;

class Donor extends Model
{
    protected $fillable = [
        'title',
        'slug',
        'image'
    ];

    public const GALLERY_PATH = 'public/images/donors/';

    public function grants(): HasMany
    {
        return $this->hasMany(Grant::class);
    }

    public function getMenuThumbnailAttribute(): ?string
    {
        return Helpers::getImageCache(
            Storage::url(self::GALLERY_PATH . $this->id . "/" . $this->image),
            100,
            100
        );
    }

    public function getThumbnailInnerAttribute(): ?string
    {
        return Helpers::getImageCache(
            Storage::url(self::GALLERY_PATH . $this->id . "/" . $this->image),
            140,
            42
        );
    }
}
