<?php

namespace App\Models\Category;

use App\Helpers\Helpers;
use App\Models\Grant\Grant;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Facades\Storage;

class Category extends Model
{
    protected $fillable = [
        'title',
        'slug',
        'icoclass_id'
    ];

    public function categories_grants(): BelongsToMany
    {
        return $this->belongsToMany(Grant::class);
    }

    public function icoclass(): BelongsTo
    {
        return $this->belongsTo(Icoclass::class);
    }

    public function getThumbnailAttribute()
    {
        return Helpers::getImageCache(
            Storage::disk('categories')->url($this->id . '/' . $this->image),
            239,
            239
        );
    }
}
