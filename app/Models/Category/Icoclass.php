<?php

namespace App\Models\Category;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Icoclass extends Model
{
    protected $fillable = [
        'name'
    ];

    public function categories(): HasMany
    {
        return $this->hasMany(Category::class);
    }
}
