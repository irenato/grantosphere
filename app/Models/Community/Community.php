<?php

namespace App\Models\Community;

use App\Helpers\Helpers;
use App\Models\Grant\Grant;
use App\Models\Region\Region;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Storage;

class Community extends Authenticatable
{
    use Notifiable;

    public const GUARD_NAME = 'community';

    public const GALLERY_PATH = 'public/images/communities/';

    protected $fillable = [
        'title',
        'email',
        'head_full_name',
        'logo',
        'phone',
        'active',
        'region_id',
        'notified_at',
        'activation_hash'
    ];

    public function region(): BelongsTo
    {
        return $this->belongsTo(Region::class);
    }

    public function communities_grants(): BelongsToMany
    {
        return $this->belongsToMany(Grant::class);
    }

    public function getMenuThumbnailAttribute(): ?string
    {
        return Helpers::getImageCache(
            Storage::url(self::GALLERY_PATH . $this->id . "/" . $this->logo),
            100,
            100
        );
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }
}
