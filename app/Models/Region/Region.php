<?php

namespace App\Models\Region;

use App\Models\Community\Community;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Region extends Model
{
    protected $fillable = [
        'title'
    ];

    public function communities(): HasMany
    {
        return $this->hasMany(Community::class);
    }
}
