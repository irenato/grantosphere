<?php

namespace App\Models\Grant;

use App\Helpers\Helpers;
use App\Models\Category\Category;
use App\Models\Community\Community;
use App\Models\Donor\Donor;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Facades\Storage;

class Grant extends Model
{
    protected $fillable = [
        'title',
        'slug',
        'image',
        'description',
        'active',
        'donor_id',
        'cost',
        'deadline',
        'source_link',
        'views'
    ];

    public const GALLERY_PATH = 'public/images/grants/';

    public function donor(): BelongsTo
    {
        return $this->BelongsTo(Donor::class);
    }

    public function categories_grants(): BelongsToMany
    {
        return $this->belongsToMany(Category::class);
    }

    public function communities_grants(): BelongsToMany
    {
        return $this->belongsToMany(Community::class);
    }

    public function getMenuThumbnailAttribute(): ?string
    {
        return Helpers::getImageCache(
            Storage::url(self::GALLERY_PATH . $this->id . "/" . $this->image),
            100,
            100
        );
    }

    public function getThumbnailAttribute(): ?string
    {
        return Helpers::getImageCache(
            Storage::url(self::GALLERY_PATH . $this->id . "/" . $this->image),
            130,
            100
        );
    }

    public function getThumbnailInnerAttribute(): ?string
    {
        return Helpers::getImageCache(
            Storage::url(self::GALLERY_PATH . $this->id . "/" . $this->image),
            460,
            280
        );
    }

    public function getDeadlineDateAttribute()
    {
        return Carbon::parse($this->deadline)->format('d.m.y');
    }
}
