<?php

namespace App\Http\Controllers\Frontend\Favourite;

use App\Helpers\Helpers;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FavouriteController extends Controller
{
    public function index(Request $request)
    {
        $grants = Helpers::getUser()
            ->communities_grants()
            ->with(['donor', 'categories_grants', 'categories_grants.icoclass'])
            ->where('deadline', '>', date('Y-m-d H:i:s'));
        if ($request->has('order')) {
            $grants->orderBy($request->order);
        }
        return view('frontend.profile.favourites.index', [
            'grants' => $grants->get()
        ]);
    }

    public function add(int $id): ?JsonResponse
    {
        Helpers::getUser()->communities_grants()->attach($id);
        return response()->json([
            'data' => view('frontend.parts.buttons.unlike', [
                'grantId' => $id
            ])->render()
        ]);
    }

    public function remove(int $id): ?JsonResponse
    {
        if (Helpers::getUser()->communities_grants()->detach($id)) {
            return response()->json([
                'data' => view('frontend.parts.buttons.like', [
                    'grantId' => $id
                ])->render()
            ]);
        }
        return null;
    }
}
