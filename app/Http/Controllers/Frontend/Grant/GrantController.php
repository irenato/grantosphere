<?php

namespace App\Http\Controllers\Frontend\Grant;

use App\Helpers\Helpers;
use App\Models\Community\Community;
use App\Models\Grant\Grant;
use App\Http\Controllers\Controller;

class GrantController extends Controller
{
    public function index(?string $slug)
    {
        $grant = Grant::with([
            'donor',
            'categories_grants',
            'categories_grants.icoclass',
            'communities_grants'
        ])
            ->where('deadline', '>', date('Y-m-d H:i:s'))
            ->whereSlug($slug)
            ->firstOrFail();
        $grant->increment('views');
        return view('frontend.grant.index', [
            'grant' => $grant,
            'likeSection' => view(
                'frontend.parts.buttons.' . (
                Helpers::checkIsFavourite($grant)
                    ? 'unlike'
                    : 'like'
                ),
                ['grantId' => $grant->id]
            )
        ]);
    }
}
