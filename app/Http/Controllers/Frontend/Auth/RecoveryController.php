<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Http\Requests\Frontend\Auth\RecoveryPasswordRequest;
use App\Models\Community\Community;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;

class RecoveryController extends Controller
{
    use SendsPasswordResetEmails;

    protected const GUARD_NAME = Community::GUARD_NAME;

    protected $redirectTo = 'frontend.login';
    protected $redirectHome = 'frontend.home';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function index()
    {
        if (auth()->guard(self::GUARD_NAME)->check()) {
            return redirect(route($this->redirectHome));
        }
        return view('frontend.auth.recovery');
    }

    public function sendResetLinkEmail(RecoveryPasswordRequest $request)
    {
        $response = $this->broker()->sendResetLink(
            $this->credentials($request)
        );
        return $response == Password::RESET_LINK_SENT
            ? $this->sendResetLinkResponse($request, $response)
            : $this->sendResetLinkFailedResponse($request, $response);
    }

    protected function broker()
    {
        return Password::broker('communities');
    }
}
