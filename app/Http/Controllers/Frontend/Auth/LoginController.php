<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Http\Requests\Frontend\Auth\LoginRequest;
use App\Models\Community\Community;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected const GUARD_NAME = Community::GUARD_NAME;

    protected $redirectTo = 'frontend.login';
    protected $redirectHome = 'frontend.home';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function loginForm()
    {
        if (auth()->guard(self::GUARD_NAME)->check()) {
            return redirect(route($this->redirectHome));
        }
        return view('frontend.auth.login');
    }

    public function login(LoginRequest $request)
    {
        auth()->guard(self::GUARD_NAME)
            ->attempt([
                'email' => $request->email,
                'password' => $request->password
            ]);
        if (auth()->guard(self::GUARD_NAME)->check()) {
            return redirect(route($this->redirectHome));
        }
        return redirect(route($this->redirectTo))->withErrors( __('frontend.incorrect_password'));
    }

    public function logout(): RedirectResponse
    {
        auth()->guard(self::GUARD_NAME)->logout();
        return redirect(route($this->redirectTo));
    }
}
