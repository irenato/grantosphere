<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Http\Requests\Frontend\Auth\ResetPasswordRequest;
use App\Models\Community\Community;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RegistrationController extends Controller
{

    protected $redirectTo = 'frontend.login';
    protected $redirectHome = 'frontend.home';

    public function registration(Request $request, ?string $hash)
    {
        $community = Community::where('activation_hash', $hash)->firstOrFail();
        if (isset($community)) {
            return view('frontend.registration.setPassword', [
                'hash' => $hash
            ]);
        }
        return redirect(route($this->redirectTo));
    }

    public function setPassword(ResetPasswordRequest $request): RedirectResponse
    {
        $community = Community::where('activation_hash', $request->hash)->firstOrFail();
        $community->activation_hash = null;
        $community->password = bcrypt($request->password);
        $community->active = true;
        $community->update();
        auth()->guard(Community::GUARD_NAME)
            ->attempt([
                'email' => $community->email,
                'password' => $request->password
            ]);
        return redirect(route($this->redirectHome));
    }
}
