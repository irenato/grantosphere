<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Http\Requests\Frontend\Auth\ResetPasswordRequest;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;

class ResetPasswordController extends Controller
{
    use ResetsPasswords;

    protected $redirectTo = '/';

    public function showResetForm(Request $request, string $token = null)
    {
        return view('frontend.auth.reset')->with([
                'token' => $request->token,
                'email' => $request->email
            ]
        );
    }

    public function reset(ResetPasswordRequest $request)
    {
        $request->request->set('password_confirmation', $request->password);
        $response = $this->broker()->reset(
            $this->credentials($request), function ($user, $password) {
            $this->resetPassword($user, $password);
        }
        );
        return $response == Password::PASSWORD_RESET
            ? $this->sendResetResponse($request, $response)
            : $this->sendResetFailedResponse($request, $response);
    }

    protected function broker()
    {
        return Password::broker('communities');
    }
}
