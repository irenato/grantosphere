<?php

namespace App\Http\Controllers\Frontend\Search;

use App\Models\Category\Category;
use App\Http\Controllers\Controller;
use App\Models\Grant\Grant;
use Illuminate\Http\Request;
use Illuminate\View\View;

class SearchController extends Controller
{
    public function index(): View
    {
        return view('frontend.search.index', [
            'categories' => Category::orderBy('title')
                ->get()
                ->pluck('title', 'slug')
        ]);
    }

    public function search(Request $request)
    {
        if ($request->has('category')) {
            $category = Category::whereSlug($request->category)->firstOrFail();
            $grants = Grant::with(['donor', 'categories_grants', 'categories_grants.icoclass'])
                ->where('deadline', '>', date('Y-m-d H:i:s'))
                ->whereHas('categories_grants', function ($query) use ($category) {
                    $query->where('category_id', $category->id);
                });
            if ($request->has('order')) {
                $grants->orderBy($request->order);
            }
            return view('frontend.search.result', [
                'category' => $category,
                'grants' => $grants->get()
            ]);
        }
        abort(404);
    }
}
