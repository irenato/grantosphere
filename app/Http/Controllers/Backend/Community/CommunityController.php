<?php

namespace App\Http\Controllers\Backend\Community;

use App\Http\Requests\Backend\CommunityRequest;
use App\Models\Community\Community;
use App\Models\Region\Region;
use App\Repositories\Community\CommunityRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;

class CommunityController extends Controller
{

    protected $repository;

    public function __construct(Request $request)
    {
        $this->repository = new CommunityRepository($request);
    }

    public function index(): View
    {
        return view('backend.communities.index', [
            'regions' => Region::query()->orderBy('title')->pluck('title', 'id'),
            'communities' => $this->repository->index()
        ]);
    }

    public function create(): View
    {
        return view('backend.communities.create', [
            'regions' => Region::query()->orderBy('title')->pluck('title', 'id')
        ]);
    }

    public function store(CommunityRequest $request): RedirectResponse
    {
        return redirect(
            route('backend.communities.edit', ['community' => $this->repository->store()]))
            ->with('success', __('backend.created'));
    }

    public function edit($id): View
    {
        $community = Community::with(['region'])->findOrFail($id);
        return view('backend.communities.edit', [
            'community' => $community,
            'regions' => Region::query()->orderBy('title')->pluck('title', 'id')
        ]);
    }

    public function update(CommunityRequest $request, $id): RedirectResponse
    {
        return redirect(
            route('backend.communities.edit', ['grant' => $this->repository->update($id)]))
            ->with('success', __('backend.updated'));
    }

    public function destroy($id)
    {
        $community = Community::findOrFail($id);
        Storage::deleteDirectory(Community::GALLERY_PATH . $community->id);
        $community->delete();
        return redirect()
            ->route('backend.communities.index')->with('success', __('backend.deleted'));
    }

    public function sendRegLink(Request $request): RedirectResponse
    {
        $this->repository->sendRegLink();
        return redirect(
            route('backend.communities.index'))
            ->with('success', __('backend.mail_sended'));
    }
}
