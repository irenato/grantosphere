<?php

namespace App\Http\Controllers\Backend\Region;

use App\Http\Requests\Backend\CategoryRequest;
use App\Http\Requests\Backend\RegionRequest;
use App\Models\Category\Category;
use App\Models\Region\Region;
use App\Repositories\Region\RegionRepository;
use App\Repositories\Region\UserRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

class RegionController extends Controller
{
    protected const LIMIT = 50;
    protected $repository;

    public function __construct(Request $request)
    {
        $this->repository = new RegionRepository($request);
    }

    public function index(Request $request): View
    {
        $regions = Region::query()->when(
            $request->has('search'), function ($query) use ($request) {
            return $query->where('title', 'LIKE', "%" . $request->search . "%");
        });
        return view('backend.regions.index', [
            'regions' => $regions->latest('updated_at')->paginate(self::LIMIT)
        ]);
    }

    public function create(): View
    {
        return view('backend.regions.create');
    }

    public function store(RegionRequest $request): RedirectResponse
    {
        return redirect(
            route('backend.regions.edit', ['region' => $this->repository->store()]))
            ->with('success', __('backend.created'));
    }

    public function edit($id)
    {
        return view('backend.regions.edit', [
            'region' => Region::findOrFail($id)
        ]);
    }

    public function update(RegionRequest $request, $id): RedirectResponse
    {
        return redirect(
            route('backend.regions.edit', ['category' => $this->repository->update($id)]))
            ->with('success', __('backend.updated'));
    }

    public function destroy($id): RedirectResponse
    {
        Region::findOrFail($id)->delete();
        return redirect(
            route('backend.regions.index'))
            ->with('success', __('backend.deleted'));
    }
}
