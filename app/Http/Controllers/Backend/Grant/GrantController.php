<?php

namespace App\Http\Controllers\Backend\Grant;

use App\Http\Requests\Backend\GrantRequest;
use App\Models\Category\Category;
use App\Models\Donor\Donor;
use App\Models\Grant\Grant;
use App\Repositories\Grant\GrantRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;

class GrantController extends Controller
{
    protected $repository;

    public function __construct(Request $request)
    {
        $this->repository = new GrantRepository($request);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request): View
    {
        $basicGrants = $this->repository->index();
        $grants = clone ($basicGrants)->each(function (Grant $grant) {
            $grant->categories_grants = $grant->categories_grants()->pluck('title')->implode(', ');
            return $grant;
        });
        return view('backend.grants.index', [
            'categories' => Category::all()->pluck('title', 'id'),
            'donors' => Donor::all()->pluck('title', 'id'),
            'grants' => $grants,
            'basicGrants' => $basicGrants
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(): View
    {
        return view('backend.grants.create', [
            'categories' => Category::all()->pluck('title', 'id'),
            'donors' => Donor::all()->pluck('title', 'id')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(GrantRequest $request): RedirectResponse
    {
        return redirect(
            route('backend.grants.edit', ['grant' => $this->repository->store()]))
            ->with('success', __('backend.created'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id): View
    {
        $grant = Grant::with(['categories_grants'])->findOrFail($id);
        return view('backend.grants.edit', [
            'grant' => $grant,
            'categories' => Category::all()->pluck('title', 'id'),
            'donors' => Donor::all()->pluck('title', 'id'),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(GrantRequest $request, $id): RedirectResponse
    {
        return redirect(
            route('backend.grants.edit', ['grant' => $this->repository->update($id)]))
            ->with('success', __('backend.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $grant = Grant::findOrFail($id);
        Storage::deleteDirectory(Grant::GALLERY_PATH . $grant->id);
        $grant->delete();
        return redirect()
            ->route('backend.grants.index')->with('success', __('backend.deleted'));
    }
}
