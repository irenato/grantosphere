<?php

namespace App\Http\Controllers\Backend\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\ProfileRequest;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{


    public function edit($id)
    {
        if($id != auth()->guard()->user()->id){
            return redirect()->back()->with('error', 'forbidden');
        }
        return view('backend.profile.edit', [
            'user'  => User::find($id),
        ]);
    }

    public function update(ProfileRequest $request, $id)
    {
        $user        = User::find($id);
        $user->name  = $request->name;
        $user->email = $request->email;
        if (isset($request->password)) {
            $user->password = Hash::make($request->password);
        }
        $user->update();
        return redirect()->back()->with('success', __('backend.updated'));
    }
}
