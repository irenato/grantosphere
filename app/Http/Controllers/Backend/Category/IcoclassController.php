<?php

namespace App\Http\Controllers\Backend\Category;

use App\Http\Requests\Backend\IcoclassRequest;
use App\Models\Category\Category;
use App\Models\Category\Icoclass;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

class IcoclassController extends Controller
{
    protected const LIMIT = 50;

    public function index(Request $request): View
    {
        $icons = Icoclass::query()->when(
            $request->has('search'), function ($query) use ($request) {
            return $query->where('title', 'LIKE', "%" . $request->search . "%");
        });
        return view('backend.icons.index', [
            'icons' => $icons->latest('updated_at')->paginate(self::LIMIT)
        ]);
    }

    public function create(): View
    {
        return view('backend.icons.create', [
            'icons' => Icoclass::all()->pluck('name', 'id')
        ]);
    }

    public function store(IcoclassRequest $request): RedirectResponse
    {
        return redirect(
            route('backend.icons.edit', ['icoclass' => Icoclass::create($request->all())]))
            ->with('success', __('backend.created'));
    }


    public function edit($id): View
    {
        return view('backend.icons.edit', [
            'icon' => Icoclass::findOrFail($id)
        ]);
    }

    public function update(IcoclassRequest $request, $id): RedirectResponse
    {
        $icon = Icoclass::findOrFail($id);
        $icon->update($request->all());
        return redirect(
            route('backend.icons.edit', ['icoclass' => $icon]))
            ->with('success', __('backend.updated'));
    }

    public function destroy($id): RedirectResponse
    {
        Icoclass::findOrFail($id)->delete();
        return redirect(
            route('backend.icons.index'))
            ->with('success', __('backend.deleted'));
    }
}
