<?php

namespace App\Http\Controllers\Backend\Category;

use App\Http\Requests\Backend\CategoryRequest;
use App\Models\Category\Category;
use App\Models\Category\Icoclass;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\Category\RegionRepository;
use App\Repositories\Category\DonorRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

class CategoryController extends Controller
{
    protected const LIMIT = 50;
    protected $repository;

    public function __construct(Request $request)
    {
        $this->repository = new CategoryRepository($request);
    }

    public function index(Request $request): View
    {
        $categories = Category::query()->when(
            $request->has('search'), function ($query) use ($request) {
            return $query->where('title', 'LIKE', "%" . $request->search . "%");
        });
        return view('backend.categories.index', [
            'categories' => $categories->latest('updated_at')->paginate(self::LIMIT)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(): View
    {
        return view('backend.categories.create', [
            'icoclasses' => Icoclass::all()->pluck('name', 'id')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request): RedirectResponse
    {
        return redirect(
            route('backend.categories.edit', ['category' => $this->repository->store()]))
            ->with('success', __('backend.created'));
    }

    public function edit($id)
    {
        return view('backend.categories.edit', [
            'category' => Category::findOrFail($id),
            'icoclasses' => Icoclass::all()->pluck('name', 'id')
        ]);
    }

    public function update(CategoryRequest $request, $id): RedirectResponse
    {
        return redirect(
            route('backend.categories.edit', ['category' => $this->repository->update($id)]))
            ->with('success', __('backend.updated'));
    }

    public function destroy($id): RedirectResponse
    {
        Category::findOrFail($id)->delete();
        return redirect(
            route('backend.categories.index'))
            ->with('success', __('backend.deleted'));
    }
}
