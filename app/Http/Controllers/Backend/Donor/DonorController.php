<?php

namespace App\Http\Controllers\Backend\Donor;

use App\Http\Requests\Backend\DonorRequest;
use App\Models\Donor\Donor;
use App\Repositories\Donor\DonorRepository;
use App\Repositories\Grant\GrantRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;

class DonorController extends Controller
{
    protected const LIMIT = 50;
    protected $repository;

    public function __construct(Request $request)
    {
        $this->repository = new DonorRepository($request);
    }

    public function index(Request $request): View
    {
        $donors = Donor::query()->when(
            $request->has('search'), function ($query) use ($request) {
            return $query->where('title', 'LIKE', "%" . $request->search . "%");
        });
        return view('backend.donors.index', [
            'donors' => $donors->latest('updated_at')->paginate(self::LIMIT)
        ]);
    }

    public function create(): View
    {
        return view('backend.donors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(DonorRequest $request): RedirectResponse
    {
        return redirect(
            route('backend.donors.edit', ['donor' => $this->repository->store()]))
            ->with('success', __('backend.created'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id): View
    {
        return view('backend.donors.edit', [
            'donor' => Donor::findOrFail($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(DonorRequest $request, $id): RedirectResponse
    {
        return redirect(
            route('backend.donors.edit', ['donor' => $this->repository->update($id)]))
            ->with('success', __('backend.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $donor = Donor::findOrFail($id);
        Storage::deleteDirectory(Donor::GALLERY_PATH . $donor->id);
        $donor->delete();
        return redirect()
            ->route('backend.donors.index')->with('success', __('backend.deleted'));
    }
}
