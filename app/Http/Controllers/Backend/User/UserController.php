<?php

namespace App\Http\Controllers\Backend\User;

use App\Http\Requests\Backend\UserRequest;
use App\Models\User;
use App\Repositories\User\UserRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\View\View;

class UserController extends Controller
{
    protected const LIMIT = 50;
    protected $repository;

    public function __construct(Request $request)
    {
        $this->repository = new UserRepository($request);
    }

    public function index(Request $request): View
    {
        $users = User::query();
        if ($request->all()) {
            foreach ($request->all() as $key => $value) {
                if (isset($value))
                    switch ($key) {
                        case 'page':
                            break;
                        case 'active':
                            $users
                                ->where('active', $value);
                            break;
                        default:
                            $users = $users
                                ->where($key, 'like', "%" . $value . "%");
                            break;
                    }
            }
        }
        return view('backend.users.index', [
            'users' => $users->paginate(50),
        ]);
    }

    public function create(): View
    {
        return view('backend.users.create');
    }

    public function store(UserRequest $request): RedirectResponse
    {
        return redirect(
            route('backend.users.edit', ['user' => $this->repository->store()]))
            ->with('success', __('backend.created'));
    }

    public function edit($id): View
    {
        return view('backend.users.edit', [
            'user' => User::find($id),
        ]);
    }

    public function update(UserRequest $request, $id): RedirectResponse
    {
        return redirect(
            route('backend.users.edit', ['user' => $this->repository->update($id)]))
            ->with('success', __('backend.updated'));
    }

    public function destroy($id): RedirectResponse
    {
        if (auth()->guard()->user()->id == $id) {
            return redirect(route('backend.users.edit', ['user' => $id]))
                ->with('error', __('backend.you_can_not_delete_yourself'));
        }
        $user = User::findOrFail($id);
        $user->delete();
        return redirect('backend/users/')
            ->with('success', __('backend.deleted'));
    }
}
