<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CommunityMiddleware
{
    public function handle($request, Closure $next)
    {
        if (!Auth::guard('community')->check()) {
            return redirect(route('frontend.login'));
        }

        return $next($request);
    }
}
