<?php

namespace App\Helpers;

use App\Models\Community\Community;
use App\Models\Grant\Grant;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class Helpers
{
    public static function getImageCache($path = '', $width = 50, $height = 50, $folder = null): string
    {
        if (!File::exists(public_path() . $path)
            || !is_file(public_path() . $path)
        ) {
            $path = '/assets/images/noimage.png';
        }
        $ext = explode('.', $path);
        if (end($ext) == 'svg') {
            return $path;
        }
        $name = md5($path . $width . $height) . '.' . end($ext);
        if (!File::exists(public_path() . '/assets/images/cache')) {
            File::makeDirectory(public_path() . '/assets/images/cache', 0777);
        }
        if ($folder != null
            && !File::exists(public_path() . '/assets/images/cache/' . $folder)
        ) {
            File::makeDirectory(public_path() . '/assets/images/cache/'
                . $folder, 0777);
        }
        $filename = public_path() . '/assets/images/cache/' . ($folder != null
                ? $folder . '/' : '') . $name;
        if (!File::exists($filename)) {
            Image::make(public_path() . '/' . $path)->fit($width, $height)
                ->save($filename);
        }
        return asset('/assets/images/cache/' . ($folder != null ? $folder . '/' : '') . $name);
    }

    public static function checkIsFavourite(Grant $grant): bool
    {
        return $grant
            ->communities_grants()
            ->pluck('community_id')
            ->contains(
                auth()
                    ->guard(Community::GUARD_NAME)
                    ->user()
                    ->id
            );
    }

    public static function getUser(): Community
    {
        return auth()->guard(Community::GUARD_NAME)->user();
    }

}
