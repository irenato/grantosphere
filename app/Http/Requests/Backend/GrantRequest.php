<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class GrantRequest extends FormRequest
{
    const BEGIN_TIMESTAMP = '1970-01-01 00:00';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|unique:grants,title,' . $this->route('grant'),
            'slug' => 'nullable|string|regex:/^[-a-z0-9]+$/|unique:grants,slug,' . $this->route('grant'),
            'image' => 'file|image|mimes:jpeg,jpg,png|max:2000',
            'donor_id' => 'required|numeric',
            'categories_grants' => 'required|array|min:1',
            'cost' => 'required|min:1|max:7|regex:/^\d*(\.\d{1,2})?$/',
            'source_link' => 'nullable|string|max:128',
            'deadline' => 'required|date||after:' . self::BEGIN_TIMESTAMP,
        ];
    }
}
