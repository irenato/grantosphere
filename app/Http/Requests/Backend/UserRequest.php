<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name'     => 'required|min:3',
            'email'    => 'required|email|unique:users,email,' . $this->route('user'),
            'password' => 'nullable|min:3',
        ];
    }
}
