<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class CommunityRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'title' => 'required|string|min:3|max:64|unique:communities,title,' . $this->route('community'),
            'email' => 'required|string|min:3|max:64|unique:communities,email,' . $this->route('community'),
            'head_full_name' => 'required|string|min:3|max:64',
            'phone' => 'required|min:3|max:64',
            'logo' => 'file|image|mimes:jpeg,jpg,png|max:2000'
        ];
    }
}
