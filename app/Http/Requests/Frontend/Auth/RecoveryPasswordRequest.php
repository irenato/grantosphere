<?php

namespace App\Http\Requests\Frontend\Auth;

use Illuminate\Foundation\Http\FormRequest;

class RecoveryPasswordRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email' => 'required|max:64|exists:communities,email,active,1',
        ];
    }

    public function messages(): array
    {
        return [
            'email.required' => __('frontend.email_required'),
            'email.exists' => __('frontend.user_disabled_or_not_exists')
        ];
    }
}
