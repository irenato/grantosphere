<?php

namespace App\Http\Requests\Frontend\Auth;

use App\Models\Community\Community;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;

class LoginRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'email' => 'required|max:64|exists:communities,email,active,1',
            'password' => 'required',
        ];
    }

    public function withValidator($validator)
    {
        if (request()->isMethod('post') && isset($this->email)) {
            $validator->after(function ($validator) {
                $community = Community::whereEmail($this->email)->get();
                if (isset($community->password) && !Hash::check($this->password, $community->password)) {
                    $validator->errors()->add('password', __('frontend.incorrect_password'));
                }
            });
        }
        return;
    }

    public function messages(): array
    {
        return [
            'email.required' => __('frontend.email_required'),
            'email.exists' => __('frontend.user_disabled_or_not_exists'),
            'password.required' => __('frontend.password_required'),
        ];
    }
}
