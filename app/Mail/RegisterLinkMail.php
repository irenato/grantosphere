<?php

namespace App\Mail;

use App\Models\Community\Community;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegisterLinkMail extends Mailable
{
    use Queueable, SerializesModels;

    public $community;

    public function __construct(Community $community)
    {
        $this->community = $community;
    }

    public function build()
    {
        return $this->markdown('backend.mails.register-link-mail');
    }
}
