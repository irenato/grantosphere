<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGrantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grants', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('donor_id')->unsigned()->nullable();
            $table->string('title', 191)->index();
            $table->string('slug', 191)->index();
            $table->string('source_link', 191)->nullable();
            $table->text('description');
            $table->string('image')->nullable();
            $table->double('cost', 15, 8);
            $table->dateTime('deadline')->nullable();
            $table->boolean('active')->default(false);
            $table->integer('position')->default(0);
            $table->timestamps();
            $table->unique('title');
            $table->unique('slug');
            $table->foreign('donor_id')
                ->references('id')
                ->on('donors')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grants');
    }
}
