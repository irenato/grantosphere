<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCommunitiesTableAddNotifiedAtActivationHashColumns extends Migration
{
    public function up(): void
    {
        Schema::table('communities', function (Blueprint $table): void {
            $table->dateTime('notified_at')->nullable();
            $table->string('activation_hash', 32)->index()->nullable();
        });
    }

    public function down(): void
    {
        Schema::table('communities', function (Blueprint $table): void {
            $table->dropColumn([
                'notified_at',
                'activation_hash'
            ]);
        });
    }
}
