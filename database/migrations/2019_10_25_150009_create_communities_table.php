<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommunitiesTable extends Migration
{
    private const STRING_LENGTH = 64;
    public function up()
    {
        Schema::create('communities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('region_id')->unsigned()->nullable();
            $table->string('title', self::STRING_LENGTH);
            $table->string('email', self::STRING_LENGTH);
            $table->string('head_full_name', self::STRING_LENGTH);
            $table->string('logo')->nullable();
            $table->string('password')->nullable();
            $table->char('phone', 24)->nullable()->default(null);
            $table->boolean('active')->default(true);
            $table->rememberToken();
            $table->timestamps();
            $table->unique('title');
            $table->unique('email');
            $table->foreign('region_id')
                ->references('id')
                ->on('regions')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('communities');
    }
}
