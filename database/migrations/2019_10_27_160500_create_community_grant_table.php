<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommunityGrantTable extends Migration
{
    public function up(): void
    {
        Schema::create('community_grant', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('community_id')->unsigned();
            $table->bigInteger('grant_id')->unsigned();
            $table->timestamps();
            $table->unique([
                'community_id',
                'grant_id'
            ]);
            $table->foreign('community_id')
                ->references('id')
                ->on('communities');
            $table->foreign('grant_id')
                ->references('id')
                ->on('grants');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('community_grant');
    }
}
