<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterGrantsTableAddViewsColumn extends Migration
{
    public function up(): void
    {
        Schema::table('grants', function (Blueprint $table): void {
            $table->bigInteger('views')->unsigned()->default(0);
        });
    }

    public function down(): void
    {
        Schema::table('grants', function (Blueprint $table): void {
            $table->dropColumn('views');
        });
    }
}
