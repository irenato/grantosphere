<?php

use App\Models\Category\Category;
use App\Models\Grant\Grant;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class GrantTableSeeder extends Seeder
{
    public function run(): void
    {
        $categories = Category::all()->pluck('id');
        $faker = Faker\Factory::create();
        Storage::deleteDirectory(Grant::GALLERY_PATH);
        factory(Grant::class, 132)
            ->create()
            ->each(function (Grant $grant) use ($faker, $categories): void {
                $grant->categories_grants()->sync($categories->random());
                Storage::makeDirectory(Grant::GALLERY_PATH . $grant->id, 755);
                $grant->image = $faker->image(
                    storage_path('app/' . Grant::GALLERY_PATH . $grant->id), 400, 300, 'city',
                    false);
                $grant->update();
            });
    }
}
