<?php

use App\Models\Community\Community;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class CommunityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $faker = Faker\Factory::create();
        Storage::deleteDirectory(Community::GALLERY_PATH);
        factory(Community::class, 3)
            ->create()
            ->each(function (Community $community) use ($faker) {
                Storage::makeDirectory(Community::GALLERY_PATH . $community->id, 755);
                $community->logo = $faker->image(
                    storage_path('app/' . Community::GALLERY_PATH . $community->id), 400, 300, 'city',
                    false);
                $community->update();
            });
    }
}
