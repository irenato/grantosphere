<?php

use App\Models\Category\Icoclass;
use Illuminate\Database\Seeder;

class IcoclassTableSeeder extends Seeder
{
    public function run(): void
    {
        Icoclass::create(['name' => 'icon-dollar']);
    }
}
