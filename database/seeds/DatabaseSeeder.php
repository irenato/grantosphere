<?php

use App\Models\Category\Category;
use App\Models\Community\Community;
use App\Models\Donor\Donor;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(IcoclassTableSeeder::class);
        $this->call(CategoryTableSeeder::class);
        $this->call(DonorTableSeeder::class);
        $this->call(GrantTableSeeder::class);
        $this->call(RegionTableSeeder::class);
        $this->call(CommunityTableSeeder::class);
    }
}
