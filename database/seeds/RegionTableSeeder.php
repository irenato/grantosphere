<?php

use App\Models\Region\Region;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class RegionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $regionData = json_decode(
            file_get_contents(
                storage_path('data/regions.json')
            ),
            true
        );
        try {
            foreach ($regionData['areas'] as $region) {
                Region::create([
                    'title' => $region['name']
                ]);
            }
        } catch (ErrorException $e) {
            echo $e->getMessage();
        }
    }
}
