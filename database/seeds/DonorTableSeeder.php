<?php

use App\Models\Donor\Donor;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class DonorTableSeeder extends Seeder
{
    public function run(): void
    {
        $faker = Faker\Factory::create();
        Storage::deleteDirectory(Donor::GALLERY_PATH);
        factory(Donor::class, 12)
            ->create()
            ->each(function (Donor $donor) use ($faker) {
                Storage::makeDirectory(Donor::GALLERY_PATH . $donor->id, 755);
                $donor->image = $faker->image(
                    storage_path('app/' . Donor::GALLERY_PATH . $donor->id), 400, 300, 'city',
                    false);
                $donor->update();
            });
    }
}
