<?php

use App\Models\Category\Category;
use App\Models\Category\Icoclass;
use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    public function run(): void
    {
        factory(Category::class, 12)->create();
    }
}
