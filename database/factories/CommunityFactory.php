<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Models\Community\Community;
use App\Models\Region\Region;
use Faker\Generator as Faker;

$factory->define(Community::class, function (Faker $faker) {
    $email = $faker->unique()->email;
    return [
        'title' => $faker->unique()->jobTitle,
        'email' => $email,
        'activation_hash' => md5($email),
        'active' => $faker->boolean,
        'phone' => $faker->phoneNumber,
        'region_id' => Region::inRandomOrder()->first()->id,
        'head_full_name' => $faker->firstName . ' ' . $faker->lastName,
        'password' => bcrypt('123456')
    ];
});
