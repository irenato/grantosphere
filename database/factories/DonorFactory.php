<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Models\Donor\Donor;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Donor::class, function (Faker $faker): array {
    $title = $faker->unique()->company;
    return [
        'title' => $title,
        'slug' => Str::slug($title),
    ];
});
