<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Models\Donor\Donor;
use App\Models\Grant\Grant;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Grant::class, function (Faker $faker): array {
    $title = $faker->unique()->jobTitle;
    return [
        'title' => $title,
        'slug' => Str::slug($title),
        'description' => $faker->sentence(128),
        'active' => $faker->boolean,
        'cost' => $faker->randomFloat(null, 2, 3),
        'source_link' => $faker->url,
        'deadline' => $faker->dateTimeBetween('now', '+1 year'),
        'donor_id' => Donor::inRandomOrder()->first()->id
    ];
});
