<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Models\Category\Category;
use App\Models\Category\Icoclass;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$icoClass = Icoclass::first();
$factory->define(Category::class, function (Faker $faker) use ($icoClass): array {
    $title = $faker->unique()->colorName;
    return [
        'title' => $title,
        'slug' => Str::slug($title),
        'icoclass_id' => $icoClass->id
    ];
});
