## Installation

1. Clone repository

2. Copy `.env.example` to `.env`

3. [Install Docker](https://docs.docker.com/install/)

4. [Install docker-compose](https://docs.docker.com/compose/install/)

5. Run `docker-compose build && docker-compose up -d`
 
6. Enter to container `docker exec -it gsph-php bash`

7. Update packages: `composer install`

8. Generate unique key for laravel: `php artisan key:generate`

9. Link to storage:  `php artisan storage:link`

10. Do migrations & seeds:  `php artisan migrate:fresh --seed`

11. Clear cache `php:artisan clear:cache`

**Admin**
* `{siteUrl}/backend`
* login: `admin@example.com`
* pass: `123456`
