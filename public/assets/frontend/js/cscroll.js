$('.info_grant, .grants_content').customScroll({
    prefix: 'custom-scroll_',

    /* vertical */
    barMinHeight: 10,
    offsetTop: 0,
    offsetBottom: 0,
    /* will be added to offsetBottom in case of horizontal scroll */
    trackWidth: 100,

    /* horizontal */
    barMinWidth: 10,
    offsetLeft: 0,
    offsetRight: 0,
    /* will be added to offsetRight in case of vertical scroll */
    trackHeight: 10,

    /* each bar will have custom-scroll_bar-x or y class */
    barHtml: '<div />',
    trackHtml: '<div />',

    /* both vertical or horizontal bar can be disabled */
    vertical: true,
    horizontal: false
});
