$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$(document).ready(function () {
    $(document).on('click', '.js-like', function (e) {
        var _this = $(this);
        e.preventDefault();
        $.ajax({
            'url': _this.attr('href'),
            'method': 'POST',
            'success': function (response) {
                if (typeof response.data !== 'undefined') {
                    var parent = $(_this).closest('.js-parent')
                    $(_this).detach();
                    $(parent).prepend(response.data);
                }
            }
        });
    })
    $(document).on('change', '.js-order-input', function () {
        $(this).closest('form').submit();
    })
});
