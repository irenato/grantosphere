
module.exports = function (gulp, plugins) {
    return function () {
        gulp.src('assets/frontend/src/sass/*.scss')
            .pipe(plugins.sass())
            .pipe(plugins.cleanCss())
            .pipe(gulp.dest('assets/frontend/css'));
    };
};