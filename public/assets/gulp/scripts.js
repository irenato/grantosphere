module.exports = function (gulp, plugins) {
    return function () {
        gulp.src('assets/frontend/src/js/*.js')
            .pipe(plugins.terser())
            .pipe(gulp.dest('assets/frontend/js'));
    };
};