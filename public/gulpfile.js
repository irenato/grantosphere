var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();



gulp.task('scripts', require('./assets/gulp/scripts')(gulp, plugins));
gulp.task('sass', require('.//assets/gulp/sass')(gulp, plugins));

gulp.task('default', ['sass', 'scripts'], function () {
    gulp.watch('assets/frontend/src/js/**/*.js', ['scripts']);
    gulp.watch('assets/frontend/src/sass/**/*.{sass,scss}', ['sass']);
});