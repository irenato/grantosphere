$(document).ready(function () {
    $('.js-remove').on('click', function () {
        $('.js-destroy').trigger('click');
        return false;
    })

    $('.js-image-link').on('click', function (e) {
        e.preventDefault();
        $(this).closest('.js-parent').find('input:file').trigger('click');
    })

    $(".js-thumbnail").change(function () {
        readImg(this);
    });

    if ($('.js-chosen').length > 0) {
        $('.js-chosen').chosen();
    }

    if ($('.datetimepicker').length > 0) {
        $('.datetimepicker').datetimepicker({
            format: 'yyyy-mm-dd hh:ii:ss',
            autoclose: true
        });
    }
})

function readImg(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $(input).closest('.js-parent').find('img').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
