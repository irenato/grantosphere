<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'backend', 'as' => 'backend.'], function () {
    Route::group(['middleware' => ['guest']], function () {
        Route::post('/', 'Auth\LoginController@login')->name('auth');
        Route::get('/', 'Auth\LoginController@loginForm')->name('login');
    });
    Route::group(['middleware' => ['auth']], function () {
        Route::get('/dashboard', 'Backend\Dashboard\DashboardController@index')->name('dashboard');
        Route::resource('/categories', 'Backend\Category\CategoryController');
        Route::resource('/donors', 'Backend\Donor\DonorController');
        Route::resource('/grants', 'Backend\Grant\GrantController');
        Route::resource('/icons', 'Backend\Category\IcoclassController');
        Route::resource('/regions', 'Backend\Region\RegionController');
        Route::get('/communities/send-reg-link', 'Backend\Community\CommunityController@sendRegLink')->name('communities.sendRegLink');
        Route::resource('/communities', 'Backend\Community\CommunityController');
        Route::resource('/users', 'Backend\User\UserController');
        Route::resource('profile', 'Backend\Profile\ProfileController');
        Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
    });
});
