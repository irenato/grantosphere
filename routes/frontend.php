<?php

use Illuminate\Support\Facades\Route;

Route::get('/password-reset', 'Frontend\Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::group(['as' => 'frontend.'], function () {
    Route::group(['middleware' => ['guest']], function () {
        Route::post('/', 'Frontend\Auth\LoginController@login')->name('auth');
        Route::get('/registration/{hash}', 'Frontend\Auth\RegistrationController@registration')->name('registration');
        Route::post('/setPassword/', 'Frontend\Auth\RegistrationController@setPassword')->name('setPassword');
        Route::get('/login', 'Frontend\Auth\LoginController@loginForm')->name('login');
        Route::get('/logout', 'Frontend\Auth\LoginController@logout')->name('logout');
        Route::get('/recovery', 'Frontend\Auth\RecoveryController@index')->name('recoveryForm');
        Route::post('/recovery', 'Frontend\Auth\RecoveryController@sendResetLinkEmail')->name('sendResetLink');
        Route::post('/reset', 'Frontend\Auth\ResetPasswordController@reset')->name('reset');
    });
    Route::group(['middleware' => ['community']], function () {
        Route::get('/', 'Frontend\Search\SearchController@index')->name('home');
        Route::group(['as' => 'search.'], function () {
            Route::get('/search', 'Frontend\Search\SearchController@index')->name('index');
            Route::get('/search/results', 'Frontend\Search\SearchController@search')->name('search');
        });
        Route::group(['as' => 'grant.'], function () {
            Route::get('grant/{slug}', 'Frontend\Grant\GrantController@index')->name('index');
        });
        Route::group(['as' => 'favourites.', 'prefix' => 'favourites'], function () {
            Route::get('/', 'Frontend\Favourite\FavouriteController@index')->name('index');
            Route::post('/add/{id}', 'Frontend\Favourite\FavouriteController@add')->name('add');
            Route::post('/remove/{id}', 'Frontend\Favourite\FavouriteController@remove')->name('remove');
        });
    });
});
