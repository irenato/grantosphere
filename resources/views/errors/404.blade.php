@extends('frontend.layouts.app')
@section('bodyClass')
    m_auth
@stop
@section('title')
    <title>@lang('frontend.set_password')</title>
@stop
<div class="content">
    <img class="icon" src="/assets/img/icon.png">
    <div class="g_info flex flex-column flex-space-between">
        <span>@lang('frontend.page_not_found')</span>
    </div>
    <a href="{{ route('frontend.home') }}" type="submit">@lang('frontend.to_main')</a>
</div>
