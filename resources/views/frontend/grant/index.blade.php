@extends('frontend.layouts.app')
@section('title')
    <title>{{ $grant->title }}</title>
@stop

@section('bodyClass') grant @stop
<div class="content ">

    <h1>@lang('frontend.grant_title')</h1>

    <div class="grant_content">
        <div class="right_content">
            <div class="flex flex-column">
                <div class="img_grant" style="background-image: url('{{ $grant->thumbnailInner }}')"></div>
                <div>
                    <span>{{ $grant->cost }} @lang('frontend.uah')</span>
                </div>
                <div class="flex flex-space-between ">
                    <div class="g_category">@foreach($grant->categories_grants as $category)
                            <span class="{{ $category->icoclass->name }}"></span> {{ $category->title }}
                        @endforeach</div>
                    <div class="g_time"><span class="icon-time"></span> {{ $grant->deadlineDate }}</div>
                </div>
                <div class="partner_logo">
                    <img src="{{ $grant->donor->thumbnailInner }}" alt="{{ $grant->donor->title }}">
                </div>
            </div>

        </div>
        <div class="left_content">
            <h2>{{ $grant->title }}</h2>
            <div class="info_grant">
                {{ $grant->description }}

            </div>
        </div>
    </div>
    <div class="btn_grant">

        <button class="back_result" onclick="location.href='{{ route('frontend.search.index') }}';"><span
                class="icon-arrow-left"></span> @lang('frontend.to_search')</button>
        <div class="btn_favorite js-parent">
            {!! $likeSection !!}
            <a href="{{ $grant->source_link }}" target="_blank">
                <button class="submite">@lang('frontend.apply')</button>
            </a>
        </div>
    </div>
</div>

