<div class="sort">
    {{ Form::open(['url' => route($routeName), 'method' => 'GET']) }}
    {!! Form::select('order', $values, request('order' ?? ''), ['class' => 'js-order-input']) !!}
    {!! Form::hidden('category', $category) !!}
    {{ Form::close() }}
</div>
