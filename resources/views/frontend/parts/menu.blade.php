<div id="menu" class="hidden">

    <div class="flex logo_block">
        <div class="logo" style="background-image: url({{ $community->menuThumbnail }})"></div>
        <h2>{{ $community->title }}</h2>
    </div>

    <div class="menu_list">
        <ul>
            <li><span class="icon-musica-searcher"></span> <a href="{{ route('frontend.search.index') }}">@lang('frontend.search_grants')</a></li>
            <li><span class="icon-star"></span> <a href="{{ route('frontend.favourites.index') }}">@lang('frontend.my_grants')</a></li>
            <li><span class="icon-info-sign"></span> <a href="#">@lang('frontend.how_to_write_grant')</a></li>
            <li><span class="icon-option"></span> <a href="#">@lang('frontend.help')</a></li>
        </ul>
    </div>

</div>
