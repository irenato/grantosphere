<a href="{{ route('frontend.favourites.remove', ['id' => $grantId]) }}" class="js-like">
    <button class="like">
        <span class="icon-star"></span>@lang('frontend.remove_from_favourites')
    </button>
</a>
