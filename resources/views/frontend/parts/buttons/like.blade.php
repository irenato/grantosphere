<a href="{{ route('frontend.favourites.add', ['id' => $grantId]) }}" class="js-like">
    <button class="like">
        <span class="icon-star-empty"></span>@lang('frontend.add_to_favourites')
    </button>
</a>
