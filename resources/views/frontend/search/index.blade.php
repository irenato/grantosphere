@extends('frontend.layouts.app')
@section('bodyClass')
    search
@stop
@section('title')
    <title>@lang('frontend.search')</title>
@stop
<div class="content">
    <div class="flex flex-column">
        <h1>@lang('frontend.search')</h1>
        {{ Form::open(['url' => route('frontend.search.search'), 'method' => 'GET', 'class' => 'search']) }}
        <div class="flex ">
            <div class="chosen-wrapper" data-js="custom-scroll">
                {!! Form::select('category', $categories, null, ['class' => 'chosen-select']) !!}
            </div>
            <button type="submit">@lang('frontend.search') <span class="icon-search-white"></span></button>
        </div>
        {{ Form::close() }}
    </div>
</div>

@section('scripts')
    <script src="{{ asset('/assets/frontend/js/jquery.nicescroll.min.js') }}"></script>
@stop
