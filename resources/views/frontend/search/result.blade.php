@extends('frontend.layouts.app')
@section('title')
    <title>@lang('frontend.search')</title>
@stop
@section('bodyClass') result @stop
<div class="content">
    <div class="flex flex-column">
        <div class="flex flex-space-between flex-middle">
            <div class="flex flex-column">
                <h1>@lang('frontend.search_result')</h1>
                <h4>{{ $category->title }} ({{ $grants->count() }})</h4>
            </div>
            @include('frontend.parts.forms.order-results')
        </div>

        <div class="grants_content">
            @forelse($grants as $grant)
                <div class="single_grant">
                    <div class="g_img" style="background: url('{{ $grant->thumbnail }}') "></div>
                    <div class="g_info flex flex-column flex-space-between">
                        <div>
                            <a href="{{ route('frontend.grant.index', ['slug' => $grant->slug]) }}">{{ $grant->title }}</a>
                        </div>
                        <div class="flex flex-space-between">
                            <div class="g_category">
                                @foreach($grant->categories_grants as $category)
                                    <span class="{{ $category->icoclass->name }}"></span> {{ $category->title }}
                                @endforeach
                            </div>
                            <div class="g_time"><span class="icon-time"></span> {{ $grant->deadlineDate }}</div>
                        </div>
                    </div>
                </div>
            @empty
                @lang('frontend.not_found')
            @endforelse
        </div>

        <button onclick="location.href='{{ route('frontend.search.index') }}';"><span
                class="icon-arrow-left"></span> @lang('frontend.to_search')</button>

    </div>


</div>
