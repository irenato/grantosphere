<!DOCTYPE html>
<html lang="ua">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ asset('/assets/frontend/css/index.css') }}" rel="stylesheet">
    @yield('styles')

    @yield('pagetitle')
</head>

<body class="@yield('bodyClass')">
<header>
    @if(Auth::guard('community')->check())
        <div>
            <div id="toggle">
                <div class="one"></div>
                <div class="two"></div>
                <div class="three"></div>
            </div>
        </div>
        <div>
            <a class="reminder"><span class="icon-vector"></span></a>
            <a class="out" href="{{ route('frontend.logout') }}"><span class="icon-out"></span></a>
        </div>
    @endif
</header>
@if(Auth::guard('community')->check())
    @include('frontend.parts.menu')
@endif

@yield('content')

<script
    src="https://code.jquery.com/jquery-3.4.1.min.js"
    integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
    crossorigin="anonymous"></script>
@yield('scripts')
<script src="{{ asset('/assets/frontend/js/custom_scroll.js') }}"></script>
<script src="{{ asset('/assets/frontend/js/cscroll.js') }}"></script>
<script src="{{ asset('/assets/frontend/js/index.js') }}"></script>
<script src="{{ asset('/assets/frontend/js/common.js') }}"></script>


</body>
</html>
