@extends('frontend.layouts.app')
@section('bodyClass')
    m_auth
@stop
@section('title')
    <title>@lang('frontend.set_password')</title>
@stop
<div class="content">
    <img class="icon" src="/assets/img/icon.png">

    {{ Form::open(['url' => route('frontend.setPassword'), 'method' => 'POST', 'autocomplete' => 'off']) }}
    {{ Form::password('password', null, ["placeholder" => __('frontend.empty_pass'), 'class' => '_jsvalidation'])}}
    {{ Form::hidden('hash', $hash)}}
    <button type="submit">@lang('frontend.save')</button>
    {{ Form::close() }}
</div>

@section('scripts')
    <script type="text/javascript" src="{{ url('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('\App\Http\Requests\Frontend\Auth\ResetPasswordRequest') !!}
@endsection
