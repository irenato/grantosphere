@extends('frontend.layouts.app')
@section('bodyClass')
    m_auth
@stop
@section('title')
    <title>@lang('frontend.login')</title>
@stop
<div class="content">
    <img class="icon" src={{ "assets/img/icon.png" }}>

    {{ Form::open(['url' => route('frontend.auth'), 'method' => 'POST', 'autocomplete' => 'off']) }}
        {{ Form::email('email', old('email'), ["placeholder" => __('frontend.empty_email'), 'class' => '_jsvalidation'])}}
        {{ Form::password('password', null, ["placeholder" => __('frontend.empty_pass'), 'class' => '_jsvalidation'])}}
    @if (!$errors->isEmpty())
        <span class="invalid-feedback">{!! $errors->first() !!}</span>
    @endif

        <button type="submit">@lang('frontend.login')</button>
    {{ Form::close() }}

    <a href="{{ route('frontend.recoveryForm') }}" class="forgot">@lang('frontend.forgot_pass')</a>

</div>

@section('scripts')
    <script type="text/javascript" src="{{ url('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('\App\Http\Requests\Frontend\Auth\LoginRequest') !!}
@endsection
