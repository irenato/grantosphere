@extends('frontend.layouts.app')
@section('bodyClass')
    m_auth
@stop
@section('title')
    <title>@lang('frontend.recovery_form')</title>
@stop
<div class="content">
    <img class="icon" src={{ "assets/img/icon.png" }}>

    {{ Form::open(['url' => route('frontend.sendResetLink'), 'method' => 'POST', 'autocomplete' => 'off']) }}
    {{ Form::email('email', old('email'), ["placeholder" => __('frontend.empty_email'), 'class' => '_jsvalidation'])}}

    <button type="submit">@lang('frontend.send')</button>
    {{ Form::close() }}
    <a href="{{ route('frontend.login') }}" class="forgot">@lang('frontend.enter')</a>

</div>

@section('scripts')
    <script type="text/javascript" src="{{ url('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('\App\Http\Requests\Frontend\Auth\RecoveryPasswordRequest') !!}
@endsection
