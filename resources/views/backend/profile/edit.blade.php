@extends('backend.layouts.app')
@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">@lang('backend.edit')</h1>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1">
                <li>
                    <a href="{{ route('backend.dashboard') }}"><i class="fa-home"></i>@lang('backend.main')</a>
                </li>
                <li class="active">
                    <strong>@lang('backend.edit')</strong>
                </li>
            </ol>
        </div>
    </div>
@endsection
@section('content')
    {{ Form::open(['url' => route('backend.profile.update',['id'=> $user->id]), 'method' => 'PUT', 'class' => 'validate form-horizontal js_form', "novalidate" => 'novalidate', 'autocomplete' => 'off']) }}
    <div class="panel panel-headerless">
        <div class="panel-body">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#main" data-toggle="tab">
                        <span class="visible-xs"><i class="fa-user"></i></span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="main">

                    <div class="member-form-inputs js_parent_div" data-csrf="{{ csrf_token() }}">
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('name', 'Name *', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    {{  Form::text('name', $user->name, ['class' => 'form-control', 'data-validate' => 'required', "placeholder" => 'Имя'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('email', 'Email', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    {{  Form::text('email', $user->email, ['class' => 'form-control', "placeholder" => 'Email'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('password', 'Пароль *', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    {{  Form::password('password', null, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Телефон'])}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="form-group">
                <a href="{{ route('backend.dashboard') }}" class="btn btn-white"><span
                        class="fa-arrow-left"></span> @lang('backend.back')</a>
                <button type="submit" class="btn btn-success js_form_submit"><span class="fa-save"></span>
                    <b>@lang('backend.save')</b>
                </button>
            </div>
        </div>
    </div>
    {{ Form::close() }}
    s

@endsection

