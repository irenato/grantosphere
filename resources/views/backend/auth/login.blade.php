@extends('backend.layouts.login')
@section('content')
    <script type="text/javascript">
        jQuery(document).ready(function($)
        {
            // Reveal Login form
            setTimeout(function(){ $(".fade-in-effect").addClass('in'); }, 1);


            // Validation and Ajax action
            $("form#login").validate({
                rules: {
                    username: {
                        required: true
                    },

                    passwd: {
                        required: true
                    }
                },

                messages: {
                    username: {
                        required: 'Please enter your username.'
                    },

                    passwd: {
                        required: 'Please enter your password.'
                    }
                },

                // Form Processing via AJAX
                submitHandler: function(form)
                {
                    show_loading_bar(70); // Fill progress bar to 70% (just a given value)

                    var opts = {
                        "closeButton": true,
                        "debug": false,
                        "positionClass": "toast-top-full-width",
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    };

                    $.ajax({
                        url: '{!! route('backend.auth') !!}',
                        method: 'post',
                        datatype: 'json',
                        data: {
                            email: $(form).find('#username').val(),
                            pwd: $(form).find('#passwd').val(),
                            _token: '{{csrf_token()}}',
                        },
                        success: function(resp)
                        {
                            if(resp.auth == true) {
                                window.location.href = "/backend/dashboard";
                            } else {
                                switch(resp.auth) {
                                    case 'block':
                                        _mess = 'ваш акаунт заблокирован.';
                                        break;
                                    case 'black':
                                        _mess = 'ваш ip адрес заблокирован.';
                                        break;
                                    default:
                                        _mess = 'введеные данные неправильные. попробуйте еще.';
                                        break;
                                }
                                show_loading_bar({
                                    delay: .5,
                                    pct: 100,
                                    finish: function () {
                                            toastr.error(_mess, "ошибка!", opts);
                                            $(form).find('#passwd').select();
                                    }
                                });
                            }
                        },
                        error: function(resp)
                        {
                            if(resp.auth == true) {
                                window.location.href = "/backend/dashboard";
                            } else {
                                switch(resp.auth) {
                                    case 'block':
                                        _mess = 'ваш акаунт заблокирован.';
                                        break;
                                    case 'black':
                                        _mess = 'ваш ip адрес заблокирован.';
                                        break;
                                    default:
                                        _mess = 'введеные данные неправильные. попробуйте еще.';
                                        break;
                                }
                                show_loading_bar({
                                    delay: .5,
                                    pct: 100,
                                    finish: function () {
                                        toastr.error(_mess, "ошибка!", opts);
                                        $(form).find('#passwd').select();
                                    }
                                });
                            }
                        }
                    });

                }
            });

            // set form focus
            $("form#login .form-group:has(.form-control):first .form-control").focus();
        });
    </script>

    <!-- errors container -->
    <div class="errors-container">
    </div>

    <!-- add class "fade-in-effect" for login form effect -->
    <form method="post" role="form" id="login" class="login-form fade-in-effect">

        <div class="login-header">
            <span>sign-in</span>
        </div>

        <div class="form-group">
            <label class="control-label" for="username">username</label>
            <input type="text" class="form-control" name="username" id="username" autocomplete="off" />
        </div>

        <div class="form-group">
            <label class="control-label" for="passwd">password</label>
            <input type="password" class="form-control" name="passwd" id="passwd" autocomplete="off" />
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-dark  btn-block text-left">
                <i class="fa-lock"></i>
                log in
            </button>
        </div>
    </form>
@endsection
