<ul id="main-menu" class="main-menu">
    <li>
        <a href="{{ route('backend.grants.index') }}">
            <i class="fa-book"></i>
            <span class="title">@lang('backend.grants')</span>
        </a>
    </li>
    <li>
        <a href="#">
            <i class="fa-paragraph"></i>
            <span class="title">@lang('backend.categories')</span>
        </a>
        <ul>
            <li>
                <a href="{{ route('backend.categories.index') }}">
                    <i class="fa-paragraph"></i>
                    <span class="title">@lang('backend.categories')</span>
                </a>
            </li>
            <li>
                <a href="{{ route('backend.icons.index') }}">
                    <i class="fa-html5"></i>
                    <span class="title">@lang('backend.icoclasses')</span>
                </a>
            </li>
        </ul>
    </li>
    <li>
        <a href="{{ route('backend.donors.index') }}">
            <i class="fa-anchor"></i>
            <span class="title">@lang('backend.donors')</span>
        </a>
    </li>
    <li>
        <a href="{{ route('backend.regions.index') }}">
            <i class="fa-map-marker"></i>
            <span class="title">@lang('backend.regions')</span>
        </a>
    </li>
    <li>
        <a href="{{ route('backend.communities.index') }}">
            <i class="fa-child"></i>
            <span class="title">@lang('backend.communities')</span>
        </a>
    </li>
    <li>
        <a href="#">
            <i class="fa-key"></i>
            <span class="title">@lang('backend.settings')</span>
        </a>
        <ul>
            <li>
                <a href="{{ route('backend.users.index') }}">
                    <i class="fa-rocket"></i>
                    <span class="title">@lang('backend.users')</span>
                </a>
            </li>
        </ul>
    </li>
</ul>
