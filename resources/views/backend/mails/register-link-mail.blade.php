@component('mail::panel')
    Реєстрація
@endcomponent
@component('mail::message')
    Шановний(на) {{ $community->head_full_name }}.
    Запрошуємо Вашу громаду "{{ $community->title }}" приєднатись до нашого проекту Грантосфера.
    Реєстрація видбудеться за посиланням:
    @component('mail::button', ['url' => route('frontend.registration', ['hash' => $community->activation_hash]), 'color' => 'success'])
        Перейти
    @endcomponent
@endcomponent
