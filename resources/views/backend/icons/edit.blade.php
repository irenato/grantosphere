@extends('backend.layouts.app')
@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">@lang('backend.edit_category')</h1>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1">
                <li>
                    <a href="{{ route('backend.dashboard') }}"><i class="fa-home"></i>@lang('backend.home')</a>
                </li>
                <li>
                    <a href="{{ route('backend.icons.index') }}">@lang('backend.icoclasses')</a>
                </li>
                <li class="active">
                    <strong>@lang('backend.edit_category')</strong>
                </li>
            </ol>
        </div>
    </div>
@endsection
@section('content')
    {!! Form::model($icon, ['url' => route('backend.icons.update',['id'=> $icon->id]), 'novalidate' => 'novalidate', 'method'=>'PUT','autocomplete'=>'off', 'files' => true]) !!}
    @include('backend.icons.fields')
    {!! Form::close() !!}
    <div class="hidden">
        {!! Form::open(['route' => ['backend.icons.destroy', $icon->id], 'method' => 'delete']) !!}
        {!! Form::submit(__('backend.delete'), ['class' => 'btn btn-danger js-destroy', 'onclick' => "return confirm('" . __('backend.are_you_sure') . "')"]) !!}
        {!! Form::close() !!}
    </div>
@endsection
