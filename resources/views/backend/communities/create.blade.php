@extends('backend.layouts.app')
@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">@lang('backend.add_grant')</h1>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1">
                <li>
                    <a href="{{ route('backend.dashboard') }}"><i class="fa-home"></i>@lang('backend.home')</a>
                </li>
                <li>
                    <a href="{{ route('backend.communities.index') }}">@lang('backend.communities')</a>
                </li>
                <li class="active">
                    <strong>@lang('backend.add_grant')</strong>
                </li>
            </ol>
        </div>
    </div>
@endsection
@section('content')
    {{ Form::open(['url' => route('backend.communities.store'), 'method' => 'POST', 'class' => 'validate', "novalidate" => 'novalidate', 'files' => true]) }}
    @include('backend.communities.fields')
    {{ Form::close() }}
@endsection
