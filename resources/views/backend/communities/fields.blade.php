<div class="panel panel-headerless">
    <div class="panel-body">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#main" data-toggle="tab">
                    <span class="visible-xs"><i class="fa-user"></i></span>
                    <span class="hidden-xs">@lang('backend.main')</span>
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="main">
                <div class="member-form-add-header">
                    <div class="row">
                        <div class="col-md-10 col-sm-8">
                            <div class="user-img js-parent">
                                <a href="#" class="js-image-link">
                                    <img src="{{ isset($community) ? $community->menuThumbnail : Helpers::getImageCache() }}"
                                         class="img-circle"
                                         alt="user-pic"/>
                                </a>
                                <div class="invisible">
                                    {{Form::file('logo', ['accept' => 'image/*', 'class' => 'js-thumbnail'])}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="member-form-inputs js_parent_div" data-csrf="{{ csrf_token() }}">
                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('title', __('backend.title') . ' *', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa-child"></i></a>
                                </div>
                                {{  Form::text('title', null, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => __('backend.title')])}}
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('email', __('backend.email'), ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa-child"></i></a>
                                </div>
                                {{  Form::email('email', null, ['class' => 'form-control', "placeholder" => __('backend.source_link')])}}
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('phone', __('backend.phone'), ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa-child"></i></a>
                                </div>
                                {{  Form::text('phone', null, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => __('backend.phone')])}}
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('head_full_name', __('backend.head_full_name') . ' *', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa-child"></i></a>
                                </div>
                                {{  Form::text('head_full_name', null, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => __('backend.title')])}}
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('region_id', __('backend.regions') . ' *', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa-child"></i></a>
                                </div>
                                {!! Form::select('region_id', $regions, null, ['class'=>'form-control select2 js-chosen', 'placeholder' => 'Choose something...']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="available">@lang('backend.active')</label>
                        <div class="col-sm-9">
                            {{  Form::hidden('active', 0)}}
                            {!! Form::checkbox('active', 1, old('active'), ['class' => 'iswitch iswitch-secondary']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="form-group">
                <a href="{{ route('backend.communities.index') }}" class="btn btn-white"><span
                        class="fa-arrow-left"></span> @lang('backend.back')</a>
                <button type="submit" class="btn btn-success js_form_submit"><span class="fa-save"></span>
                    <b>@lang('backend.save')</b>
                </button>
                @include('backend.parts.button-remove')
            </div>
        </div>
    </div>
