@extends('backend.layouts.app')

@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">@lang('backend.grants')</h1>
        </div>
        <div class="breadcrumb-env">
            <a href="{{ route('backend.communities.create') }}" class="btn btn-success"><span class="fa-plus"></span> @lang('backend.add')</a>
        </div>
        <div class="breadcrumb-env">
            <a href="{{ route('backend.communities.sendRegLink', request()->all()) }}" class="btn btn-info"><span class="fa-send"></span> @lang('backend.send_invite')</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="tab-content">
                <div class="tab-pane active" id="all">

                    <table class="table table-hover members-table middle-align">
                        <thead>
                        <tr>
                            <th>id</th>
                            <th width="20%">@lang('backend.title')</th>
                            <th width="25%">@lang('backend.head_full_name')</th>
                            <th width="20%">@lang('backend.region')</th>
                            <th width="10%">@lang('backend.image')</th>
                            <th width="10%">@lang('backend.active')</th>
                            <th>@lang('backend.action')</th>
                        </tr>
                        <tr>
                            {{ Form::open(['url' => route('backend.communities.index'), 'method' => 'GET']) }}
                            <th class="hidden-xs hidden-sm"></th>
                            <th>{{ Form::text('title', request()->{'title' ?? ''}, ['class' => 'form-control', 'autocomplete' => false, 'placeholder' => __('backend.title')]) }}</th>
                            <th>{{ Form::text('head_full_name', request()->{'head_full_name' ?? ''}, ['class' => 'form-control', 'autocomplete' => false, 'placeholder' => __('backend.head_full_name')]) }}</th>
                            <th>{!! Form::select('region_id', $regions, request('region_id' ?? ''),['class'=>'form-control js-chosen', 'placeholder' => 'Choose something...']) !!}</th>
                            <th></th>
                            <th>{!! Form::select('active', ['not active', 'active'], request('active' ?? ''),['class'=>'form-control js-chosen', 'placeholder' => 'Choose something...']) !!}</th>
                            <th>
                                <button type="submit" class="btn btn-sm btn-success">
                                    <i class="fa fa-search"></i>
                                </button>
                            </th>
                            {{ Form::close() }}
                        </tr>
                        </thead>
                        <tbody class="js-sortable" data-url="{{ url('backend/grants/sort/') }}">
                        @forelse($communities as $community)
                            <tr class="js-sortable-item" data-id="{{ $community->id }}">
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $community->id !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $community->title !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $community->head_full_name !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! isset($community->region) ? $community->region->title : '' !!}</span>
                                </td>
                                <td>
                                    <img src="{{ $community->menuThumbnail }}" alt="{{ $community->title }}">
                                </td>
                                <td>
                                    <span class="email">{!! $community->active ? 'active' : '-' !!}</span>
                                </td>
                                <td>
                                    <a href="{{ route('backend.communities.edit',['community'=> $community->id]) }}" class="btn btn-sm btn-success">
                                        <i class="linecons-pencil"></i>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6"><p class="text-center">@lang('backend.not_found')</p></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>

                    <div class="row">
                        <div class="col-sm-6">
                        </div>
                        <div class="col-sm-6 text-right text-center-sm">
                            {{ $communities->appends([request()])->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ url('assets/js/inputmask/jquery.inputmask.bundle.js') }}"></script>
@endsection
