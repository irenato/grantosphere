@if(strpos(request()->route()->getName(), 'edit'))
    <button type="submit" class="btn btn-danger js-remove"><span class="fa-save"></span>
        <b>@lang('backend.delete')</b>
    </button>
@endif
