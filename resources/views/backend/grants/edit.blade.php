@extends('backend.layouts.app')
@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">@lang('backend.edit_grant')</h1>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1">
                <li>
                    <a href="{{ route('backend.dashboard') }}"><i class="fa-home"></i>@lang('backend.home')</a>
                </li>
                <li>
                    <a href="{{ route('backend.grants.index') }}">@lang('backend.grants')</a>
                </li>
                <li class="active">
                    <strong>@lang('backend.edit_grant')</strong>
                </li>
            </ol>
        </div>
    </div>
@endsection
@section('content')
    {!! Form::model($grant, ['url' => route('backend.grants.update',['id'=> $grant->id]), 'novalidate' => 'novalidate', 'method'=>'PUT','autocomplete'=>'off', 'files' => true]) !!}
    {{ Form::hidden('oldImage', $grant->image) }}
    @include('backend.grants.fields')
    {!! Form::close() !!}
    <div class="hidden">
        {!! Form::open(['route' => ['backend.grants.destroy', $grant->id], 'method' => 'delete']) !!}
        {!! Form::submit(__('backend.delete'), ['class' => 'btn btn-danger js-destroy', 'onclick' => "return confirm('" . __('backend.are_you_sure') . "')"]) !!}
        {!! Form::close() !!}
    </div>

@endsection
