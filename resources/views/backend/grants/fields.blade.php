<div class="panel panel-headerless">
    <div class="panel-body">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#main" data-toggle="tab">
                    <span class="visible-xs"><i class="fa-user"></i></span>
                    <span class="hidden-xs">@lang('backend.main')</span>
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="main">
                <div class="member-form-add-header">
                    <div class="row">
                        <div class="col-md-10 col-sm-8">
                            <div class="user-img js-parent">
                                <a href="#" class="js-image-link">
                                    <img src="{{ isset($grant) ? $grant->menuThumbnail : Helpers::getImageCache() }}"
                                         class="img-circle"
                                         alt="user-pic"/>
                                </a>
                                <div class="invisible">
                                    {{Form::file('image', ['accept' => 'image/*', 'class' => 'js-thumbnail'])}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="member-form-inputs js_parent_div" data-csrf="{{ csrf_token() }}">
                    <div class="row form-group">
                        <div class="col-sm-2">
                            {{ Form::label('title', __('backend.title') . ' *', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa-child"></i></a>
                                </div>
                                {{  Form::text('title', null, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => __('backend.title')])}}
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-2">
                            {{ Form::label('source_link', __('backend.source_link'), ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa-child"></i></a>
                                </div>
                                {{  Form::text('source_link', null, ['class' => 'form-control', "placeholder" => __('backend.source_link')])}}
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-2">
                            {{ Form::label('deadline', __('backend.deadline') . ' *', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa-child"></i></a>
                                </div>
                                {{  Form::text('deadline', null, ['class' => 'form-control datetimepicker',  'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => __('backend.deadline')])}}
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-2">
                            {{ Form::label('slug', __('backend.alias'), ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa-child"></i></a>
                                </div>
                                {{  Form::text('slug', null, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => __('backend.alias')])}}
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-2">
                            {{ Form::label('cost', __('backend.cost') . ' *', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa-child"></i></a>
                                </div>
                                {{  Form::number('cost', null, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => __('backend.cost')])}}
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-2">
                            {{ Form::label('donors', __('backend.donors') . ' *', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa-child"></i></a>
                                </div>
                                {!! Form::select('donor_id', $donors, null, ['class'=>'form-control select2 js-chosen', 'placeholder' => 'Choose something...']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-2">
                            {{ Form::label('categories', __('backend.categories') . ' *', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa-child"></i></a>
                                </div>
                                {!! Form::select('categories_grants[]', $categories, null, ['class'=>'form-control select2 js-chosen', 'multiple' => 'multiple']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-sm-2">
                            {{ Form::label('description', __('backend.content') . ' *', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-10">
                            {!! Form::textarea('description', null, ['class'=>'form-control ckeditor js-textarea']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="form-group">
                <a href="{{ route('backend.categories.index') }}" class="btn btn-white"><span
                        class="fa-arrow-left"></span> @lang('backend.back')</a>
                <button type="submit" class="btn btn-success js_form_submit"><span class="fa-save"></span>
                    <b>@lang('backend.save')</b>
                </button>
                @include('backend.parts.button-remove')
            </div>
        </div>
    </div>

    @section('styles')
        <link rel="stylesheet" href="{{ url('assets/css/bootstrap-datetimepicker.css') }}">
    @endsection

    @section('scripts')
        <script src="{{ url('assets/js/bootstrap-datetimepicker.min.js') }}"></script>
@endsection
