@extends('backend.layouts.app')

@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">@lang('backend.grants')</h1>
        </div>
        <div class="breadcrumb-env">
            <a href="{{ route('backend.grants.create') }}" class="btn btn-success"><span class="fa-plus"></span> @lang('backend.add')</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="tab-content">
                <div class="tab-pane active" id="all">

                    <table class="table table-hover members-table middle-align">
                        <thead>
                        <tr>
                            <th>id</th>
                            <th width="20%">@lang('backend.title')</th>
                            <th width="25%">@lang('backend.donors')</th>
                            <th width="25%">@lang('backend.categories')</th>
                            <th width="15%">@lang('backend.image')</th>
                            <th>@lang('backend.action')</th>
                        </tr>
                        <tr>
                            {{ Form::open(['url' => route('backend.grants.index'), 'method' => 'GET']) }}
                            <th class="hidden-xs hidden-sm"></th>
                            <th>{{ Form::text('title', request()->{'title' ?? ''}, ['class' => 'form-control', 'autocomplete' => false, 'placeholder' => __('title')]) }}</th>
                            <th>{!! Form::select('donor', $donors, request('donor' ?? ''),['class'=>'form-control js-chosen', 'placeholder' => 'Choose something...']) !!}</th>
                            <th>{!! Form::select('category', $categories, request('category' ?? ''),['class'=>'form-control js-chosen', 'placeholder' => 'Choose something...']) !!}</th>
                            <th></th>
                            <th>
                                <button type="submit" class="btn btn-sm btn-success">
                                    <i class="fa fa-search"></i>
                                </button>
                            </th>
                            {{ Form::close() }}
                        </tr>
                        </thead>
                        <tbody class="js-sortable" data-url="{{ url('backend/grants/sort/') }}">
                        @forelse($grants as $grant)
                            <tr class="js-sortable-item" data-id="{{ $grant->id }}">
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $grant->id !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $grant->title !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $grant->donor->title !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $grant->categories_grants !!}</span>
                                </td>
                                <td>
                                    <img src="{{ $grant->menuThumbnail }}" alt="{{ $grant->title }}">
                                </td>
                                <td>
                                    <a href="{{ route('backend.grants.edit',['grant'=> $grant]) }}" class="btn btn-sm btn-success">
                                        <i class="linecons-pencil"></i>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6"><p class="text-center">@lang('backend.not_found')</p></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>

                    <div class="row">
                        <div class="col-sm-6">
                        </div>
                        <div class="col-sm-6 text-right text-center-sm">
                            {{ $basicGrants->appends([request()])->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ url('assets/js/inputmask/jquery.inputmask.bundle.js') }}"></script>
@endsection
