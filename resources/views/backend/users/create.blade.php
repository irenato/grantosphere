@extends('backend.layouts.app')
@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Добавление пользователя</h1>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1">
                <li>
                    <a href="{{ url('backend/dashboard') }}"><i class="fa-home"></i>@lang('backend.main')</a>
                </li>
                <li>
                    <a href="{{ url('backend/users') }}">@lang('backend.users')</a>
                </li>
                <li class="active">
                    <strong>@lang('backend.add')</strong>
                </li>
            </ol>
        </div>
    </div>
@endsection
@section('content')
    {{ Form::open(['url' => route('backend.users.store'), 'method' => 'POST', 'class' => 'validate', "novalidate" => 'novalidate']) }}
        @include('backend.users.fields')
    {{ Form::close() }}
@endsection

