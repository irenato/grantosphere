@extends('backend.layouts.app')
@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">@lang('backend.edit')</h1>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1">
                <li>
                    <a href="{{ url('backend/dashboard') }}"><i class="fa-home"></i>@lang('backend.main')</a>
                </li>
                <li>
                    <a href="{{ url('backend/users') }}">@lang('backend.users')</a>
                </li>
                <li class="active">
                    <strong>@lang('backend.edit')</strong>
                </li>
            </ol>
        </div>
    </div>
@endsection
@section('content')
    {{ Form::model($user, ['url' => route('backend.users.update',['id'=> $user->id]), 'method' => 'PUT', 'class' => 'validate form-horizontal js_form', "novalidate" => 'novalidate', 'autocomplete' => 'off']) }}
        @include('backend.users.fields')
    {{ Form::close() }}

    <div class="hidden">
        {!! Form::open(['route' => ['backend.users.destroy', $user->id], 'method' => 'delete']) !!}
        {!! Form::submit('Удалить', ['class' => 'btn btn-danger js-destroy', 'onclick' => "return confirm('Вы уверены?')"]) !!}
        {!! Form::close() !!}
    </div>

@endsection

