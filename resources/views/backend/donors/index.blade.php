@extends('backend.layouts.app')

@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">@lang('backend.donors')</h1>
        </div>
        <div class="breadcrumb-env">
            <a href="{{ route('backend.donors.create') }}" class="btn btn-success"><span class="fa-plus"></span> @lang('backend.add')</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="tab-content">
                <div class="tab-pane active" id="all">

                    <table class="table table-hover members-table middle-align">
                        <thead>
                        <tr>
                            <th>id</th>
                            <th width="40%">@lang('backend.title')</th>
                            <th width="40%">@lang('backend.image')</th>
                            <th>@lang('backend.action')</th>
                        </tr>
                        <tr>
                            {{ Form::open(['url' => route('backend.donors.index'), 'method' => 'GET']) }}
                            <th class="hidden-xs hidden-sm"></th>
                            <th>{{ Form::text('search', request()->{'search' ?? ''}, ['class' => 'form-control', 'autocomplete' => false, 'placeholder' => __('title')]) }}</th>
                            <th></th>
                            <th>
                                <button type="submit" class="btn btn-sm btn-success">
                                    <i class="fa fa-search"></i>
                                </button>
                            </th>
                            {{ Form::close() }}
                        </tr>
                        </thead>
                        <tbody class="js-sortable" data-url="{{ url('backend/donors/sort/') }}">
                        @forelse($donors as $donor)
                            <tr class="js-sortable-item" data-id="{{ $donor->id }}">
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $donor->id !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $donor->title !!}</span>
                                </td>
                                <td>
                                    <img src="{{ $donor->menuThumbnail }}" alt="{{ $donor->title }}">
                                </td>
                                <td>
                                    <a href="{{ route('backend.donors.edit',['grant'=> $donor]) }}" class="btn btn-sm btn-success">
                                        <i class="linecons-pencil"></i>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6"><p class="text-center">@lang('backend.not_found')</p></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>

                    <div class="row">
                        <div class="col-sm-6">
                        </div>
                        <div class="col-sm-6 text-right text-center-sm">
                            {{ $donors->appends([request()])->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ url('assets/js/inputmask/jquery.inputmask.bundle.js') }}"></script>
@endsection
